// Try to read secrets from .env file
// on other platforms we use different
// secret managers
require('dotenv').config();
const nodemailer = require('nodemailer');
const {fetchPerspectiveImage} = require('./roomle-api');
const {assertValidBody, assertValidData, assertValidEvent, getEmail, getBody, getUrl, getConfigurationId} = require('./utils');
const {SHOWCASE_WEB_HOOK_MAIL_USER_NAME: user, SHOWCASE_WEB_HOOK_MAIL_PASSWORD: pass} = process.env;
const sendMail = async (req) => {
  // Always validate the input you get so
  // that there are no security problems
  assertValidBody(req, ['event', 'data']);
  const body = getBody(req);
  assertValidEvent(body.event);
  assertValidData(body.data, ['email', 'configurationId', 'url']);
  const email = getEmail(body);
  const configurationId = getConfigurationId(body);
  // Never log out the password but we want to
  // know if it is set or not, since we use different
  // secret managers on different platforms
  console.log('Username set: %s', !!user);
  console.log('Password set: %s', !!pass);

  // Sending the mail... here you can do whatever you want
  const transporter = nodemailer.createTransport({
    host: 'email-smtp.eu-central-1.amazonaws.com',
    port: 465,
    secure: true, // use SSL
    auth: {
      user,
      pass,
    },
  });
  // Just a demo to showcase the idea
  const text = `This is a customized e-mail sent from the WebHook!
  You can do whatever you like here. You do not need to send an e-mail.
  You could also add something to the database or do whatever is best
  for your case.`;

  // for visual feedback we also include the picture of the configuration
  const perspectiveImage = await fetchPerspectiveImage(configurationId, req);
  const perspectiveImageHtml = (perspectiveImage) ? '<p><img src="' + perspectiveImage + '" alt="Configuration Image" width="480" height="480" /></p>' : '';
  const info = await transporter.sendMail({
    sender: 'ci-bot@roomle.com',
    from: '\'Showcase Roomle\' <showcase.roomle@roomle.com>',
    to: email,
    subject: 'Sent via WebHook',
    text,
    html: '<p>' + text + '</p>' + perspectiveImageHtml,
  });

  console.log('Message sent: %s', info.messageId);
};

module.exports = {
  sendMail,
};
