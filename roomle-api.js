const {SanitizedError} = require('./error');
const {getUrl, getBody} = require('./utils');
// We include the lib node-fetch so that we can fetch data
// the same way as we can do it in the browser. This is for
// convenience reasons
const fetch = (...args) => import('node-fetch').then(({default: _fetch}) => _fetch(...args));

const {SHOWCASE_WEB_HOOK_API_KEY: apiKey} = process.env;

const KNOWN_STAGES = [
  'https://api.roomle.com',
  'https://api.test.roomle.com',
  'https://api.alpha.roomle.com',
];

const CONFIGURATIONS_END_POINT = '/v2/configurations/';
const ROOMLE_STAGING_SERVER = /https:\/\/(test|alpha)\.roomle\.com/;

const getRoomleApiUrl = (req, url) => {
  if (!req) {
    throw new SanitizedError(500, 'Can not extract Roomle API Url');
  }
  if (req.headers && req.headers.origin && KNOWN_STAGES.includes(req.headers.origin)) {
    console.log('Using Roomle API Url from req.headers.origin', req.headers.origin);
    return req.headers.origin;
  }
  const matches = url.match(ROOMLE_STAGING_SERVER);
  if (!matches || matches.length <= 1) {
    console.log('No origin header present and not able to extract staging from URL, fallback to live');
    return KNOWN_STAGES[0];
  }
  const staging = (matches && matches.length > 1) ? '.' + matches[1] : '';
  const inferredUrl = 'https://api' + staging + '.roomle.com';
  console.log('Inferred "' + inferredUrl + '" as Roomle API Url');
  return inferredUrl;
};

// We are going to fetch the perspective image of the configuration
// for more details see our API docu:
// https://docs.roomle.com/rapi/RAPIDocumentation.html
const fetchPerspectiveImage = async (id, req) => {
  if (!id) {
    console.error(new Error('Response of Roomle API is not useable'));
    return null;
  }
  const roomleApiUrl = getRoomleApiUrl(req, getUrl(getBody(req)));
  const endPoint = roomleApiUrl + CONFIGURATIONS_END_POINT;
  try {
    const fetchUrl = endPoint + id;
    const response = await fetch(fetchUrl, {headers: {apiKey}});
    let json;
    try {
      json = await response.json();
    } catch (e) {
      console.error(e);
      return null;
    }
    if (json.error) {
      console.error(json.error);
      return null;
    }
    const configuration = json.configuration;
    return configuration.perspectiveImage
  } catch (e) {
    console.error(e);
    return null;
  }
};

module.exports = {
  fetchPerspectiveImage,
};
