
const {sendMail} = require('./mail');
const {SanitizedError} = require('./error');
/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
exports.mail = async (req, res) => {
  try {
    await sendMail(req);
    return res.status(200).send('Mail sent correctly');
  } catch (e) {
    // if we catch an error check if we can print the
    // error message! We do not want to print internal
    // stack traces because this could be a security
    // problem
    if (e instanceof SanitizedError) {
      return res.status(e.httpCode).send(e.message);
    } else {
      // Log generic errors so we can analyze them better
      console.error(e);
      return res.status(500).send('Problem sending mail');
    }
  }
};
